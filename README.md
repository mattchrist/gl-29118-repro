# GitLab [issue 29118](https://gitlab.com/gitlab-org/gitlab/-/issues/29118) reproduction

Note that both artifacts are downloadable from [this link](https://gitlab.com/mattchrist/gl-29118-repro/-/pipelines) by clicking the '...' action.

Only one of the artifacts can be downloaded from the API however:

## This works:

```sh
proj=32090728
job=1880211328

curl -H "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$proj/jobs/$job/artifacts/env
```

> THING=asdf


## This doesn't:
```sh
proj=32090728
job=1880211327

curl -H "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$proj/jobs/$job/artifacts/env
```
	
> {"message":"404 Not Found"}

## Differences

It seems that neither an 'archive.zip' or 'metadata.gz' artifact are uploaded by the "can't download artifacts", and 'artifact_file' is also not available.

```
$ diff <(curl -sH "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$proj/jobs/1880211328 | jq) <(curl -sH "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/$proj/jobs/1880211327 | jq)
2c2
<   "id": 1880211328,
---
>   "id": 1880211327,
5c5
<   "name": "can download artifacts",
---
>   "name": "can't download artifacts",
10,14c10,14
<   "created_at": "2021-12-13T21:01:46.513Z",
<   "started_at": "2021-12-13T21:01:48.343Z",
<   "finished_at": "2021-12-13T21:02:03.373Z",
<   "duration": 15.03078,
<   "queued_duration": 1.623213,
---
>   "created_at": "2021-12-13T21:01:46.503Z",
>   "started_at": "2021-12-13T21:01:48.378Z",
>   "finished_at": "2021-12-13T21:02:04.594Z",
>   "duration": 16.215591,
>   "queued_duration": 1.695189,
66,70c66
<   "web_url": "https://gitlab.com/mattchrist/gl-29118-repro/-/jobs/1880211328",
<   "artifacts_file": {
<     "filename": "artifacts.zip",
<     "size": 203
<   },
---
>   "web_url": "https://gitlab.com/mattchrist/gl-29118-repro/-/jobs/1880211327",
73,84d68
<       "file_type": "archive",
<       "size": 203,
<       "filename": "artifacts.zip",
<       "file_format": "zip"
<     },
<     {
<       "file_type": "metadata",
<       "size": 148,
<       "filename": "metadata.gz",
<       "file_format": "gzip"
<     },
<     {
86c70
<       "size": 2619,
---
>       "size": 2396,
98,100c82,84
<     "id": 12270831,
<     "description": "2-blue.shared.runners-manager.gitlab.com/default",
<     "ip_address": "34.75.105.58",
---
>     "id": 12270807,
>     "description": "1-blue.shared.runners-manager.gitlab.com/default",
>     "ip_address": "35.237.120.137",
108c92
<   "artifacts_expire_at": "2022-01-12T21:02:00.269Z",
---
>   "artifacts_expire_at": "2022-01-12T21:02:01.012Z",
```
